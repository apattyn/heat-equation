module Equations
    
"""
# heatspace

This the is fourier transform of the homogeneous [heat equation](https://en.wikipedia.org/wiki/Heat_equation)
with respect to the spical domain. That is to say...

Heat equation in the spacial and temporal domain:
δu/δt = α ∇²u 

becomes

Heat equation in the spacial frequency and temporal domain:
δû/δt = -α κ² û

Which is now an ODE that can be solved with traditional methods. 

## Inputs:

* u0 
    * The initial heat distribution in the fourier domain.
* params 
    * Contains α and κ
    * α
        * Thermal diffusivity [mm/sec]
    * κ
        * The spacial frequencies. 
* t 
    * Current time step [sec]

## Outputs:

    * out
        * The calculated heat distribution at a given time step.

"""
function heatspace(u0::Array{<:Number,1}, params::Array{<:Number,1}, t)
    α = params[1]
    κ = params[2]

    @. -α * κ^2 * u0

end # heatspace

function heatspace(u0::Array{<:Number,2}, params::Array{<:Number,1}, t)
    α = params[1]
    κx = params[2]
    κy = params[3]

    @. -α * ((κx^2 * u0) + ((κy')^2 * u0))

end # heatspace


end # module