module ForwardModel
    
using FFTW, DifferentialEquations

"""
# forwardmodel
"""
function forwardmodel(gridmodel::Dict)
    #==========================
    Set-up computational grid
    ==========================#
    Nx = gridmodel["Nx"] #  [grid points]
    dx = gridmodel["dx"] #  [mm]

    fmax = 1/dx          #  [1/mm]
    # We set the sampling frequency based on the Nyquist rate.
    fs = 2*fmax          #  [1/mm]

    tend = gridmodel["tend"] #  [sec]
    tspan = (0.0, tend)

    #==========================
    Initial model
    ==========================#
    # Generate spacial frequencies
    κ = fftfreq(Nx, fs)
    α = gridmodel["α"] #  [mm/sec]

    u0 = gridmodel["u0"] # should be in the spacial domain.
    u0 = fft(u0) # Transform into the spacial frequency domain.

    prob = ODEProblem()

    #==========================
    Run model
    ==========================#


end

end # ForwardModel